<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Storage;
// Image Intervention
use Intervention\Image\ImageManager;

class UserController extends Controller
{
    public $def_profile_img = 'profile.png';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('isAdmin');
        if(\Gate::allows('isAdmin') || \Gate::allows('isAuthor'))
            return User::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:191',
            'email' =>'required|unique:users|email',
            'type' => 'required|alpha',
            'photo' => 'bail',
            'bio' => 'nullable|string|max:200',
            'password' => 'required|min:6',

        ]);

        return User::Create([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'photo' => $request->photo,
            'bio' => $request->bio,
            'password' => Hash::make($request->password),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

        /**
     * Get the user profile.
     *
     * 
     * @return json
     */
    public function profile()
    {
        return auth('api')->user();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required|string|max:191',
            'email' =>'required|email|unique:users,email,' . $id, // unique except this id
            'type' => 'required|alpha',
            'photo' => 'sometimes',
            'bio' => 'sometimes|nullable|string|max:200',
            'password' => 'sometimes|min:6',

        ]);

        // Find or fail user then update it values
        $user = User::findorfail($id);

        if(! $request->has('password'))
            $user->update($request->all());
        else
        {
            $data = $request->all();
            $data['password'] = Hash::make($request->password);
            $user->update($data);

        }

    }

    /**
     * Update the specified user profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        //Get the user id from session to prevent id modification.
        $id = auth('api')->user()->id;

        $request->validate([
            'name' => 'required|string|max:191',
            'email' =>'required|email|unique:users,email,' . $id, // unique except this id
            'type' => 'required|alpha',
            'photo' => 'sometimes',
            'bio' => 'sometimes|nullable|string|max:200',
            'password' => 'sometimes|nullable|min:6',

        ]);

        // Find or fail user then update it values
        $user = User::findorfail($id);
        $current_photo = $user->photo;

        if($request->has('photo') && $request->photo != $user->photo)
        {
            $name = time() . '.'.  explode('/', explode(';', $request->photo)[0])[1];
            \Image::make($request->photo)->save(\public_path('imgs\\profile\\') . $name );
            $request->merge(['photo' =>  $name]);

            if(file_exists(\public_path('imgs\\profile\\').$current_photo))
            {
                // Use @ before unlink to prevent the func from throw error if the file not found
                @unlink(\public_path('imgs\\profile\\').$current_photo);
            }
            
        }
        
        if( $request->has('password') && $request->password != '' )
            $request->merge(['password' =>  Hash::make($request->password)]);
        else
            $request->merge(['password' =>  $user->password ]);
            
            $user->update($request->all() );

        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if user is not admin throw an error
        $this->authorize('isAdmin');
        // delete user here
        // find or fail user first
        $user = User::findorfail($id);

        $user->delete();
    }

    public function search(Request $request){ 
        return User::where('name', 'like', "%".$request->get('q')."%")
                ->orwhere('email', 'like',"%". $request->get('q') ."%")
        ->paginate(5);
    }

}
