<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('user' , 'API\UserController');

// Get the user profile
Route::get('profile', 'API\UserController@profile');

Route::get('search', 'API\UserController@search');


// Update user profile
Route::post('update_profile', 'API\UserController@updateProfile');
