
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';

import Gate from './Gate';
Vue.prototype.$gate = new Gate(window.user);

// import the date js library
import moment from 'moment';

//import vue progress bar js library
import VueProgressBar from 'vue-progressbar'
// Customize the bar properties
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '3px'
});

// Import Sweet alert2
import Swal from 'sweetalert2';

// Configure params for sweet alert
// Pop up toast window
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

// Global call Toast obj
window.Toast = Toast;
window.Swal = Swal;

//Global obj for calling vue emit
let fireEmit = new Vue();
window.fireEmit = fireEmit;

import { Form, HasError, AlertError } from 'vform';

window.Form = Form;

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.use(VueRouter);

let routes = [
    { path: '/dashboard', component: require('./components/Dashboard.vue') },
    { path: '/developers', component: require('./components/Developers.vue') },
    { path: '/profile', component: require('./components/profile.vue') },
    { path: '/users', component: require('./components/Users.vue') },


  ];
  
  // Pagination
  Vue.component('pagination', require('laravel-vue-pagination'));

  const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  });

  // Global Vue Filters //
  // upText: Captilize the first letter
  Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1) 
  });
  
  // strDate: Change the date to humand readable (Feb 15th 20)
  Vue.filter('strDate', function(date){
    return moment(date).format("MMM: Do: YY")
  });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// Passport components
Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue')
);

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component(
  'not-found',
  require('./components/NotFound.vue')
);

const app = new Vue({
    el: '#app',
    router,
    data: {
      skey: '',
    },
    methods:{
      search(){
        fireEmit.$emit('searching');
      },
    }
});
